#/bin/sh

for f in public/*.html; do
    cp head.html $f.tmp
    if [[ "$f" != "public/index.html" ]]; then
        cat header.html >> $f.tmp
    fi
    cat $f >> $f.tmp
    cat footer.html >> $f.tmp
    mv $f.tmp $f
done
